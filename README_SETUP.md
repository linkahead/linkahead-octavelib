# Getting started with CaosDB for Octave #

## Requirements ##

This package requires the following software:

- conan: `pip install conan`
- libcaosdb: Download the [libcaosdb sources](https://gitlab.com/caosdb/caosdb-cpplib) and run `make conan`.  See
  https://gitlab.indiscale.com/caosdb/src/caosdb-cpplib for further instructions.
- For running this library, a valid libcaosdb configuration is needed, for example a
  `.caosdb_client.json` file in your home directory or current working directory.  Please look at
  [`README_SETUP.md` in caosdb-cpplib](https://gitlab.indiscale.com/caosdb/src/caosdb-cpplib/-/blob/main/README_SETUP.md) for more information.
- For unit tests, MOxUnit is required, see below.

## Install and Load

* `make install`
* Or alternatively:
  * `make dist/caosdb.tar.gz`
  * Then in octave:
    * `pkg install dist/caosdb.tar.gz`
    * `pkg load caosdb;`

### Notes for installing on MacOS ###

On MacOS, installation should work the same as on Linux.  *libcaosdb* should be installed with conan
as described in [its documentation](https://gitlab.indiscale.com/caosdb/src/caosdb-cpplib).  Octave must of course also be installed, the developers
tested it with the [homebrew version](https://formulae.brew.sh/formula/octave).

## Run Unit Tests

* run `make test`

### Requirements

We use the standard test framework of Octave.

See [this article](https://wiki.octave.org/Tests) for a primer and use
[this](https://octave.org/doc/interpreter/Test-Functions.html) as a reference.

Additionally we use the [MOxUnit](https://github.com/MOxUnit/MOxUnit) framework.  Install MOxUnit somewhere:
```sh
git clone https://github.com/MOxUnit/MOxUnit.git
cd MOxUnit
make install
```


## Code Formatting

Code formatting is done with the
[miss_hit](https://florianschanda.github.io/miss_hit/index.html) tools.

1. Install `miss_hit` via  `pip install miss_hit`
2. Call `mh_style --fix --octave src/ pkg/ doc/`

Configuration of `miss_hit` is stored in `./.miss_hit`.

## Documentation ##

Build documentation in `build/doc/html` with `make doc`.

The `build/doc/html` contains a sphinx-generated, `read-the-docs`-themed,
static website. If you rather would like to use the native,
`ocatave-forge`-themed, static website, you may find it under
`doc/_build_octave`.

### Requirements ###

- `sphinx`
- `sphinx-autoapi`
- `recommonmark`
- The Octave package `generate-html`, install it with
  ```sh
octave --eval "pkg install 'https://downloads.sourceforge.net/project/octave/Octave%20Forge%20Packages/Individual%20Package%20Releases/generate_html-0.3.2.tar.gz'"
  ```
