ARG DOCKER_BASE_IMAGE
FROM $DOCKER_BASE_IMAGE

# build and install caosdb-cpplib
WORKDIR /libcaosdb/
RUN mkdir build
WORKDIR /libcaosdb/build
RUN conan create .. --build=missing -s "compiler.libcxx=libstdc++11"

RUN apt-get install -y octave-common liboctave8 octave
RUN apt-get install -y liboctave-dev
RUN apt-get install -y unzip
RUN apt-get install -y wget

# install generate-html package for octave
RUN octave --eval "pkg install 'https://downloads.sourceforge.net/project/octave/Octave%20Forge%20Packages/Individual%20Package%20Releases/generate_html-0.3.2.tar.gz'"

# Unit test framework (MOxUnit)
WORKDIR /
RUN wget --output-document MOxUnit-master.zip \
    https://github.com/MOxUnit/MOxUnit/archive/master.zip
RUN unzip MOxUnit-master.zip
WORKDIR /MOxUnit-master/
RUN make install
COPY .docker/caosdb_client.json /caosdb_client.json

COPY . /caosdb-octavelib
WORKDIR /caosdb-octavelib
RUN rm -rf .git

RUN pip3 install -U -r doc/requirements.txt
RUN pip3 install -U -r dev-requirements.txt
