@c This file is a part of the CaosDB Project.
@c
@c Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
@c Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
@c
@c This program is free software: you can redistribute it and/or modify
@c it under the terms of the GNU Affero General Public License as
@c published by the Free Software Foundation, either version 3 of the
@c License, or (at your option) any later version.
@c
@c This program is distributed in the hope that it will be useful,
@c but WITHOUT ANY WARRANTY; without even the implied warranty of
@c MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@c GNU Affero General Public License for more details.
@c
@c You should have received a copy of the GNU Affero General Public License
@c along with this program. If not, see <https://www.gnu.org/licenses/>.

@node Parents and Properties
@chapter Parents and Properties

@code{Parent} and @code{Property} objects represent the parents and properties of actual
@code{Entity} objects in Octave / Matlab.  Again, please look at the
@url{https://docs.indiscale.com/caosdb-cpplib, libcaosdb documentation} for details on the
relationships between these classes.

@c %%%%%%%%%%%%%%% Parent %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@deftp Class Parent
The parent for an Entity.
@end deftp

@c %%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%
@deftypeivar Parent string id
@end deftypeivar
@deftypeivar Parent string name
@end deftypeivar
@deftypeivar Parent string description
@end deftypeivar

@c %%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%

@defmethod Parent Parent ([struct @var{data}])

The constructor takes an optional @var{data} argument which may provide initial content.  The
typical user will just use the empty constructor @code{Parent()} and add content later.
@end defmethod

@defmethod Parent to_struct ()
@strong{Note:} This method is mostly interesting for internal use of the library, end users probably
will never need it.

Convert to a struct which has all the fields that may be needed for interaction with the backend C++
functions.
@end defmethod

@c %%%%%%%%%%%%%%% Property %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@deftp Class Property
A property of an Entity. Note that this class is for concrete property instances which are attached
to an Entity.  To create new Property entities in CaosDB, use the @ref{Entity} class with @var{role}
``@code{PROPERTY}''.
@end deftp

@c %%%%%%%%%%%%%%% properties %%%%%%%%%%%%%%%
@deftypeivar Property string id
@end deftypeivar
@deftypeivar Property string name
@end deftypeivar
@deftypeivar Property string description
@end deftypeivar
@deftypeivar Property string importance
Must be one of the supported importances ``@code{OBLIGATORY}'', ``@code{RECOMMENDED}'',
``@code{SUGGESTED}'' or ``@code{FIX}'' (see also the
@url{https://docs.indiscale.com/caosdb-server/_apidoc/org/caosdb/server/entity/StatementStatus.html,
server documentation}).
@end deftypeivar
@deftypeivar Property string unit
@end deftypeivar
@deftypeivar Property string value
The exact type of the @var{value} property should match the datatype of the Property object.
@end deftypeivar

@c %%%%%%%%%%%%%%% methods %%%%%%%%%%%%%%%

@defmethod Property Property ([struct @var{data}])
The constructor takes an optional @var{data} argument which may provide initial content.  The
typical user will just use the empty constructor @code{Provide()} and add content later.
@end defmethod

@defmethod Property get_datatype ()
@end defmethod

@defmethod Property set_datatype (string @var{dtype_name}, [logical @var{is_reference} = false, logical @var{is_list} = false])
Set the datatype of this Property in a consistent manner.

@strong{Parameters}

@param{dtype_name, string, Either the atomic datatype (for scalar simple values) or the ID of the referenced entity (in the case of reference values).}
@param{is_reference, logical, True if the value is a reference@comma{} else false (the default).}
@param{is_list, logical, True if the value is list types@comma{} false if it is a scalar (the default).}
@end defmethod

@defmethod Property to_struct ()
@strong{Note:} This method is mostly interesting for internal use of the library, end users probably
will never need it.

Convert to a struct which has all the fields that may be needed for interaction with the backend C++
functions.

If the @var{datatype} indicates a list value, the @var{value} is interpreted as such. It is an error
if the @var{value} is list-like (cell string or more than one numeric element) while the
@var{datatype} indicates a scalar value.
@end defmethod
