..
    #
    # This file is a part of the CaosDB Project.
    #
    # Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
    # Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
    #
    # This program is free software: you can redistribute it and/or modify
    # it under the terms of the GNU Affero General Public License as
    # published by the Free Software Foundation, either version 3 of the
    # License, or (at your option) any later version.
    #
    # This program is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU Affero General Public License for more details.
    #
    # You should have received a copy of the GNU Affero General Public License
    # along with this program. If not, see <https://www.gnu.org/licenses/>.
    #
    #

.. _welcome:

Welcome to LinkAhead-octavelib's documentation!
============================================

This is the documentation for the GNU Octave client library for LinkAhead, ``caosdb-octavelib``.

Note that most of the documentation is in Octave's native format.


.. toctree::
    :maxdepth: 4
    :caption: Contents:

    Welcome <self>
    Development
    Package documentation <api>
    Back to Overview <https://docs.indiscale.com/>

* :ref:`genindex`
