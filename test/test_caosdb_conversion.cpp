/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "caosdb/exceptions.h"
#include "maoxdb.hpp"
#include "mex.h"
#include <boost/lexical_cast.hpp>
#include <gtest/gtest.h>
#include <charconv>
#include <limits>
#include <memory>
#include <string>
#include <type_traits>
#include <vector>

namespace maoxdb {

using std::string;

namespace ce = caosdb::entity;

///////////////////////////////////////////////////////////////////////////////
//                              Helper functions                             //
///////////////////////////////////////////////////////////////////////////////

// Test conversion from native to mx and back
template <typename T> void test_value_native(const std::vector<T> &value_contents) {
  for (auto c : value_contents) {
    auto v = ce::Value(c);
    auto mx_v = mxFromValue(v);
    auto vv = valueFromMx(mx_v);
    EXPECT_EQ(v, vv);
  }
}

/**
 * Generate a vector with useful values for testing.
 */
template <typename T> auto generateValues() -> std::vector<T> {
  std::vector<T> values = {
    0,
    1,
    std::numeric_limits<T>::max(),
    std::numeric_limits<T>::min(),
    std::numeric_limits<T>::lowest(),
    std::numeric_limits<T>::epsilon() // 0 for integers, but who cares?
  };
  return values;
}

/**
 * Convenience wrapper around std::to_chars
 */
template <typename T> auto toStr(T value) -> std::string {
  char *chars = reinterpret_cast<char *>(std::malloc(50 * sizeof(char)));
  std::to_chars(chars, chars + 50, value);
  auto result = string(chars);
  std::free(chars);
  return result;
}

// to_chars is not widely supported for floats yet.
template <> auto toStr(double value) -> std::string {
  auto result = boost::lexical_cast<std::string>(value);
  return result;
}

///////////////////////////////////////////////////////////////////////////////
//                                Actual tests                               //
///////////////////////////////////////////////////////////////////////////////

/**
 * Test if value conversion works
 */
TEST(caosdb_conversion, value_String) {
  auto value_contents = std::vector<string>{"", "23", "foo", "CaosDB", "\n\n\n\a\n</>"};
  test_value_native(value_contents);
}

TEST(caosdb_conversion, value_Bool) {
  auto value_contents = std::vector<bool>{true, false};
  test_value_native(value_contents);
}

TEST(caosdb_conversion, value_Integer) {
  auto value_contents = generateValues<int64_t>();
  test_value_native(value_contents);
}

TEST(caosdb_conversion, value_Double) {
  auto value_contents = generateValues<double>();
  test_value_native(value_contents);
}

/**
 * Test Entity generation vom mxArray.
 */
TEST(caosdb_conversion, file_entity) {
  // clang-format off
  auto fields = std::vector<string>
    {"role",
     "id",
     "versionId",
     "name",
     "description",
     "datatype",
     "unit",
     "value",
     "parents",
     "properties",
     "filepath",
     "localpath"
    };
  // clang-format on
  auto *array = mxCreateStructFromStrings(fields);
  mxSetField(array, 0, "role", mxCreateString("FILE"));
  mxSetField(array, 0, "id", mxCreateString("-1"));
  mxSetField(array, 0, "versionId", mxEmptyCHAR());
  mxSetField(array, 0, "name", mxCreateString("File_1"));
  mxSetField(array, 0, "description", mxEmptyCHAR());
  mxSetField(array, 0, "datatype", mxEmptyCHAR());
  mxSetField(array, 0, "unit", mxEmptyCHAR());
  mxSetField(array, 0, "value", mxEmptySparse());
  mxSetField(array, 0, "filepath", mxCreateString("testfile.xyz"));
  mxSetField(array, 0, "localpath", mxCreateString("/dev/null"));
  mxSetField(array, 0, "parents", mxEmptyCell());
  mxSetField(array, 0, "properties", mxEmptyCell());
  auto entities = entitiesFromMx(array, false, "");
}
/*
 * The following are treated just like TEXT, at least for the moment:
 * - REFERENCE
 * - DATETIME
 * - References with other names
 *
 * No testing exists yet for:
 * - FILE
 */

} // namespace maoxdb
