# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# This Makefile is a wrapper for several other scripts.

.PHONY: help
help:
	@echo "Targets:"
	@echo "    pkg - create tar.gz archive of the octave package."
	@echo "    style - auto-format the source files."
	@echo "    test - run unit tests."
	@echo "    doc - create the documentation."

###############################################################################
#                        Environment dependent options                        #
###############################################################################

TAR_OPTS := undefined
ifeq "$(shell tar --version | head -n 1 | grep -o -i gnu | tr '[:upper:]' '[:lower:]')" 'gnu'
	TAR_OPTS := --exclude-ignore-recursive=.gitignore --transform 's,^\.,caosdb,'
else
	TAR_OPTS := --exclude-from=.gitignore -s '/^\./caosdb/'
endif

doc:
	$(MAKE) -C doc html
.PHONY: doc

###############################################################################
#                                   Styling                                   #
###############################################################################

style: style_octave style_cpp
.PHONY: style

style_octave:
	@mh_style --octave src doc test || ( echo 'You may want to run `make style_fix`.'; exit 1 )
.PHONY: style_octave

CLANG_FORMAT ?= clang-format-11
style_cpp:
	@$(CLANG_FORMAT) --dry-run --verbose --Werror \
    $(shell find test/ src/ -type f \
        -iname "*.cpp" -o -iname "*.hpp" -o -iname "*.h" -o -iname "*.h.in") \
    || ( echo 'You may want to run `make style_fix`.' ; exit 1 )
.PHONY: style_cpp

style_fix:
	echo "style_fix"
	mh_style --fix --octave src doc test
	$(CLANG_FORMAT) -i --verbose --Werror \
    $(shell find test/ src/ -type f \
        -iname "*.cpp" -o -iname "*.hpp" -o -iname "*.h" -o -iname "*.h.in")
.PHONY: style_fix

###############################################################################
#                                   Linting                                   #
###############################################################################

linting: linting_octave linting_cpp
.PHONY: linting

linting_octave:
	mh_lint --octave ./
.PHONY: linting_octave

linting_cpp:
	cd src                      \
    && ./configure            \
    && cd ../build            \
    && cmake -D LINTING=On .. \
    && cmake --build .
.PHONY: linting_cpp

###############################################################################
#                                    Tests                                    #
###############################################################################

test: install
	$(MAKE) -C test test
	@echo "All tests successfully passed."
.PHONY: test

test_octave: install
	$(MAKE) -C test test_octave
.PHONY: test_octave

test_cpp: install
	$(MAKE) -C test test_cpp
.PHONY: test_cpp

###############################################################################
#                          Packaging and Installation                         #
###############################################################################

.PHONY: pkg
pkg: dist/caosdb.tar.gz

.PHONY: install
install: pkg
	octave --eval "pkg install -verbose dist/caosdb.tar.gz"

.PHONY: caosdb.tar.gz
dist/caosdb.tar.gz: dist/
# For GNU tar:
# [...] --exclude-ignore-recursive=.gitignore --transform 's,^\.,caosdb,' [...]
# For POSIX tar:
# [...] --exclude-from=.gitignore -s '/^\./caosdb/' [...]
	tar -czf dist/caosdb.tar.gz \
	--exclude=.git* \
	--exclude=env* \
	--exclude=doc/_build_octave* \
	--exclude=build* \
	--exclude=.docker* \
	--exclude=dist* \
	--exclude=inst* \
	$(TAR_OPTS) \
	.

.PHONY: dist/
dist/:
	mkdir -p dist

###############################################################################
#                                   Cleaning                                  #
###############################################################################

clean: doc_clean
.PHONY: clean

doc_clean:
	rm -r doc/_build_octave/ build/doc/ NEWS || true
.PHONY: doc_clean
