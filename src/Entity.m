% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

classdef Entity < handle

  properties
    role        % One of "RECORD_TYPE", "RECORD", "PROPERTY", "FILE"
    id
    versionId
    name
    description
    unit
    value
    filepath    % Only for file entities: the file path on the server.
    localpath   % Only for file entities: the local path to the file (for upload).
  end
  properties (Access = private)
    datatype_    % 1x1 struct with the following fields:
    %  - isList         logical
    %  - isReference    logical
    %  - dtypeName      string.  Either the atomic datatype or the reference name.
    parents_     % Cell array of Parent objects. May be created from a struct array with the
    %              following fields:
    %  - id
    %  - name
    %  - description
    properties_  % Cell array of Property objects. May be created from a struct array with the
    %              following fields:
    %  - id
    %  - name
    %  - description
    %  - importance
    %  - value
    %  - unit
    %  - datatype
    errors_      % Cell array of Message objects.
    warnings_    % Like messages.
    infos_       % Like messages.
  end

  methods

    % Constructor.
    % The structure of the DATA parameter follows the convention outline in `maoxdb.hpp`.
    function obj = Entity(data)
      narginchk(0, 1);
      if nargin == 1
        assert(isstruct(data), "caosdb:InvalidArgument", "Need a struct as argument");
        obj.assign_scalars(data);
        obj.assign_parents(data);
        obj.assign_properties(data);
        obj.errors_ = obj.create_messages(data, "errors");
        obj.warnings_ = obj.create_messages(data, "warnings");
        obj.infos_ = obj.create_messages(data, "infos");
      else
        obj.parents_ = {};
        obj.properties_ = {};
        obj.errors_ = {};
        obj.warnings_ = {};
        obj.infos_ = {};
      end
    end

    % Getters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function result = get_datatype(obj)
      result = obj.datatype_;
    end

    function result = get_parents(obj)
      result = obj.parents_;
    end

    function result = get_properties(obj)
      result = obj.properties_;
    end

    function result = get_errors(obj)
      result = obj.errors_;
    end

    function result = get_warnings(obj)
      result = obj.warnings_;
    end

    function result = get_infos(obj)
      result = obj.infos_;
    end

    % Setters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set_datatype(obj, dtype_name, is_reference, is_list)
      if nargin < 4
        is_list = false;
      end
      if nargin < 3
        is_reference = false;
      end
      assert(nargin >= 2, "maox:InvalidArgument", "Need at least an atomic datatype.");

      dtype.isList = is_list;
      dtype.isReference = is_reference;
      if ~is_reference
        assert(any(strcmp({ "UNSPECIFIED", "TEXT", "DOUBLE", "DATETIME", "INTEGER", "BOOLEAN"}, ...
                          dtype_name)), ...
               "maox:InvalidArgument", ["Unrecognized atomic datatype: " dtype_name]);
      end
      dtype.dtypeName = dtype_name;
      obj.datatype_ = dtype;
    end

    function set_parents(obj, parents)
      obj.parents_ = parents;
      parents = obj.parents_;
    end

    function set_properties(obj, props)
      obj.properties_ = props;
      props = obj.properties_;
    end

    % Convenience getters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function result = get_parents_as_struct(obj)
      result = struct();
      for par = obj.parents_
        name = par{1}.name;
        if isfield(result, name)
          error("caosdb:NameCollision", ...
                "Name collision: Two or more parents of the same name, cannot pack into struct.");
        end
        result = setfield(result, name, par{1});
      end
    end

    function result = get_properties_as_struct(obj)
      result = struct();
      for prop = obj.properties_
        name = prop{1}.name;
        if isfield(result, name)
          error("caosdb:NameCollision", ...
                ["Name collision: Two or more properties of the same name, " ...
                 "cannot pack into struct."]);
        end
        result = setfield(result, name, prop{1});
      end
    end

    % Information %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function result = has_errors(obj)
      result = not(isempty(obj.errors_));
    end

    function result = has_warnings(obj)
      result = not(isempty(obj.warnings_));
    end

    function result = has_infos(obj)
      result = not(isempty(obj.infos_));
    end

    % Convert to a struct which has all the fields that may be needed for interaction with the
    % maoxdb library.
    %
    % If the datatype indicates a list value, the value is interpreted as such. It is an error if
    % the value is list-like (cell string or more than one numeric element) while the datatype
    % indicates a scalar value.
    function struct_array = to_struct(obj, warn)
      if nargin < 2
        warn = true;
      end
      res = struct();
      % Scalars first
      res.role = obj.role;
      res.id = obj.id;
      res.versionId = obj.versionId;
      res.name = obj.name;
      res.description = obj.description;
      res.datatype = obj.datatype_;
      res.unit = obj.unit;
      if isempty(obj.datatype_)
        if ~isempty(obj.value)
          warning(["Trying to transmit an Entity with VALUE but without DATATYPE.  List-iness", ...
                   " is unknown, so the VALUE will be omitted."]);
        end
        % res.datatype = struct("dtypeName", "UNSPECIFIED", "isList", false, "isReference", false);
        res.value = sparse([]);
      else
        res.value = maox_pack_value(obj.value, obj.datatype_.isList);
      end

      % file handling
      res.filepath = obj.filepath;
      res.localpath = obj.localpath;
      if warn && ~isequal(obj.role, "FILE") && (obj.filepath || obj.localpath)
        warning("caosdb:file-properties-ignored", ...
                "Setting FILEPATH or LOCALPATH makes no sense when the entity is not a FILE.");
      end
      % parents and properties
      res.parents = cellfun(@(x)(x.to_struct()), obj.get_parents());
      res.properties = cellfun(@(x)(x.to_struct()), obj.get_properties());

      struct_array = res;
    end

    function disp(obj)
      disp(obj.to_struct());
    end

  end

  methods (Access = private)

    % Only the simple metadata fields (and datatype_).
    function assign_scalars(obj, data)
      obj.role = getfielddefault(data, "role", "");
      obj.id = getfielddefault(data, "id", "");
      obj.versionId = getfielddefault(data, "versionId", "");
      obj.name = getfielddefault(data, "name", "");
      obj.description = getfielddefault(data, "description", "");
      obj.unit = getfielddefault(data, "unit", "");
      obj.value = getfielddefault(data, "value", "");
      if isfield(data, "datatype")
        obj.datatype_ = data.datatype;
      else
        obj.set_datatype("UNSPECIFIED");
      end
    end

    % Assign the parents
    function assign_parents(obj, data)
      obj.parents_ = cell();
      for parent = data.parents
        obj.parents_{end + 1} = Parent(parent);
      end
    end

    % Assign the properties
    function assign_properties(obj, data)
      obj.properties_ = cell();
      for property = getfield(data, "properties")
        obj.properties_{end + 1} = Property(property);
      end
    end

    % Create the errors, warnings, infos
    function result = create_messages(~, data, level)
      result = cell();
      if isfield(data, level)
        for message = getfield(data, level)
          result{end + 1} = Message(message);
        end
      end
    end

  end

end
