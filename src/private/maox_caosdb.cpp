/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "caosdb/connection.h" // for Connection, ConnectionManager
#include "caosdb/constants.h"  // for LIBCAOSDB_VERSION_MAJOR, LIBCAOSDB_VE...
#include "caosdb/info.h"       // for VersionInfo
#include "mex.h"               // for mxArray, mexFunction
#include <cstring>             // for strcmp
#include <memory>              // for unique_ptr, __shared_ptr_access, shar...
#include <string>              // for allocator, char_traits, operator+

auto print_version() -> const char *;
void print_usage();
void test_connection();

void print_usage() {
  print_version();
  mexPrintf("\nUsage: caosdb [OPTIONS]\n\n");
  mexPrintf("Options:\n");
  mexPrintf("    --help              Print this help and return.\n");
  mexPrintf("    --version           Print the version of OctaveCaosDB and "
            "libcaosdb and return.\n");
  mexPrintf("    --test-connection   Test the default connection and return.\n");
};

const std::string FULL_VERSION =
  std::string("v0.1 (libcaosdb v" + std::to_string(caosdb::LIBCAOSDB_VERSION_MAJOR) + "." +
              std::to_string(caosdb::LIBCAOSDB_VERSION_MINOR) + "." +
              std::to_string(caosdb::LIBCAOSDB_VERSION_PATCH) + ")");

auto print_version() -> const char * {
  mexPrintf("Octave caosdb client %s\n\n", FULL_VERSION.c_str());
  mexPrintf("We don't miss the H of caos.\n");
  return FULL_VERSION.c_str();
};

void test_connection() {
  const auto &connection = caosdb::connection::ConnectionManager::GetDefaultConnection();
  const auto &version_info = connection->GetVersionInfo();
  mexPrintf("Server Version: v%d.%d.%d-%s-%s\n", version_info->GetMajor(), version_info->GetMinor(),
            version_info->GetPatch(), version_info->GetPreRelease().c_str(),
            version_info->GetBuild().c_str());
}

void mexFunction(int /*nlhs*/, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  if (nrhs == 1) {
    auto const *first_arg = mxArrayToString(prhs[0]);
    if (strcmp(first_arg, "--version") == 0) {
      const auto *version = print_version();
      plhs[0] = mxCreateString(version);
      return;
    } else if (strcmp(first_arg, "--test-connection") == 0) {
      test_connection();
      return;
    }
  }
  print_usage();
}
