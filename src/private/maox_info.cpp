/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include "caosdb/connection.h" // for Connection, ConnectionManager
#include "caosdb/constants.h"  // for LIBCAOSDB_VERSION_MAJOR, LIBCAOSDB_VE...
#include "caosdb/exceptions.h" // for all error handling
#include "caosdb/info.h"       // for VersionInfo
#include "maoxdb.hpp"          // caosDB utils for mex files
#include "mex.h"               // for mxArray, mexFunction
#include <cstring>             // for strcmp
#include <memory>              // for unique_ptr, __shared_ptr_access, shar...
#include <string>              // for allocator, char_traits, operator+

using caosdb::connection::Connection;
using caosdb::connection::ConnectionManager;
using std::string;

const auto logger_name = "maox_info";

auto info(const string &connection_name) -> mxArray *;

/**
 * The implementation of the info retrieval.
 */
auto info(string const &connection_name) -> mxArray * {
  std::shared_ptr<Connection> connection = nullptr;
  if (connection_name.empty()) {
    connection = ConnectionManager::GetDefaultConnection();
  } else {
    connection = ConnectionManager::GetConnection(connection_name);
  }
  const auto &version_info = connection->RetrieveVersionInfo();
  const char *keys[] = {"major", "minor", "patch", "pre_release", // NOLINT
                        "build"};
  std::array<mwSize, 2> dims = {1, 1};
  mxArray *info_struct = mxCreateStructArray(2, dims.data(), 5, keys); // NOLINT

  mxSetField(info_struct, 0, "major", maoxdb::mxScalarUINT64(version_info.GetMajor()));
  mxSetField(info_struct, 0, "minor", maoxdb::mxScalarUINT64(version_info.GetMinor()));
  mxSetField(info_struct, 0, "patch", maoxdb::mxScalarUINT64(version_info.GetPatch()));
  mxSetField(info_struct, 0, "pre_release", mxCreateString(version_info.GetPreRelease().c_str()));
  mxSetField(info_struct, 0, "build", mxCreateString(version_info.GetBuild().c_str()));
  return info_struct;
}

/**
 * @brief      Get info about the server
 *
 * @details    At the moment, this is just the version of the server.
 *
 * @param connection_name A string with the connection name. May be omitted or
 * an empty string.
 *
 * @return     return type
 */
void mexFunction(int /*nlhs*/, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

  string conn_name;
  if (nrhs >= 1 && mxGetNumberOfElements(prhs[0]) > 0) {
    conn_name = mxArrayToString(prhs[0]);
  }
  try {
    auto *info_struct = info(conn_name);
    plhs[0] = info_struct;
  } catch (const caosdb::exceptions::Exception &exc) {
    mexPrintf(std::string(std::string("Exception: ") + std::string(exc.what())).c_str());
    caosdb::utility::reset_arena();
    maoxdb::throwOctException(exc);
  }
  caosdb::utility::reset_arena();
}
