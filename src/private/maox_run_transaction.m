% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

%%
% Create and execute a transaction with all possible parameters (which may be empty).
%
% This is the transaction implementation which shall be called by the simple wrappers for insert,
% update, delete.
%
% Parameters
% ----------
%
% connection_name : string
%  The connection name
%
% retrieve_ids : string cell array
%  The IDs to retrieve by ID.
%
% queries : string cell array
%  The queries to execute.
%
% inserts : struct array
%  The entities to insert, as a struct array (not Entity objects).
%
% updates : struct array
%  The entities to update, as a struct array (not Entity objects).
%
% deletes : string cell array
%  The IDs to delete.
%
% Returns
% -------
% single_result : cell array
%  The retrieved entities.  If no entities are returned and the query was a COUNT query, the result
%  is an int64 instead with the COUNT result.  If there are returned entities AND there is a count
%  result, a warning is printed and the result is a cell array with two elements: the entities and
%  the count result.  NOTE: The arguments should be so that this does not happen, and the behaviour
%  may change in the future.
%
% entities, count_results : cell array, int64
%  With two output arguments, the first is the cell array with the entities, and the second is the
%  COUNT result.  The count result is -1 if there was not exactly one COUNT query.
function [entities, count_results] = maox_run_transaction(connection_name, ...
                                                          retrieve_ids, queries, inserts, ...
                                                          updates, deletes, ...
                                                          file_ids, file_paths)
  % Boilerplate for default values
  if nargin == 0
    connection_name = "";
  end
  if nargin < 2
    retrieve_ids = {};
  end
  if nargin < 3
    queries = {};
  end
  if nargin < 4
    inserts = struct("", {});
  end
  if nargin < 5
    updates = struct("", {});
  end
  if nargin < 6
    deletes = {};
  end
  if nargin == 7
    error("maox:InvalidArgument", ...
          "If file IDs are given for download, file locations must be given as well.");
  end
  if nargin < 8
    file_ids = {};
    file_paths = {};
  end
  assert(numel(file_ids) == numel(file_paths), "maox:InvalidArgument", ...
         "Number of file IDs and file paths must be the same.");

  try
    % Retrieve and convert
    % disp(["----\nmaox_run_transaction(...) --> ", num2str(nargout()), "\n----"]);
    % disp({connection_name,  retrieve_ids, queries, ...
    %       inserts, updates, deletes});
    [collection, count_results] = maox_transaction(connection_name, retrieve_ids, queries, ...
                                                   inserts, updates, deletes, file_ids, file_paths);
    % disp("--- converting to Objects ---");
    entities = maox_convert_collection(collection);
    % disp("--- converted to Objects ---");
    % And some output argument handling
    if nargout == 1
      if count_results >= 0
        entities = count_results;

        if not(isempty(entities))
          warning("caosdb:LostValues", ...
                  ["Only the count result was returned although there were entities.  "
                   "Consider using two output arguments."]);
        end
      end
    end
  catch
    % disp("error handling in Caosdb.m");
    % disp(lasterror());
    rethrow(lasterror());
  end
end
