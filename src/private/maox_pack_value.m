% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

%% Pack the value for handling by maoxdb
%
% This means that the value will be packed into a cell array if it is considered a list.
function result = maox_pack_value(value, is_list)
  % Pack list values into a 1x1 cell array
  if is_list
    if isempty(value) && ~ischar(value)   % empty list
      result = {};
    else
      result = {value};
    end
  else  % test if value looks scalar
    if iscellstr(value) || (isnumeric(value) && numel(value) > 1)
      error("caosdb:valueError", "Value looks list-like, but it must be scalar.");
    end
    % handle empty scalar value
    if isempty(value) && ~ischar(value)
      result = sparse([]);
    else
      result = value;
    end
  end
end
