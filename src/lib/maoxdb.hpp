#ifndef MAOXDB_H // NOLINT
#define MAOXDB_H

#include "caosdb/exceptions.h"
#include "caosdb/status_code.h"
#include "caosdb/transaction.h"
#include "caosdb/transaction_status.h"
#include "caosdb/utility.h"
#include "mex.h"
#include <stdexcept>
#include <string>

/**
 * Mostly utility functions for MEX files interacting with libcaosdb.
 *
 * \b{Data exchange}
 *
 * Exchanging data between the Octave side wrapper and this C++ library is done
 * via Octave struct arrays, each struct corresponds to one CaosDB Entity.
 *
 * The Entity structs have the following structure. Not all keys need to exist for all directions,
 * when a key is only required for one direction, this is denoted by an angled bracket ("<" from
 * libcaosdb to Octave, ">" from Octave to libcaosdb).
 *
 * - role         Enum string from entity.h
 * - id           Ignored when inserting an Entity.
 * - versionId    Ignored when inserting an Entity.
 * - name
 * - description
 * - datatype     Struct with the following fields:
 *                - dtypeName:    string, either AtomicDataType from data_type.h or reference name
 *                - isReference:  logical, true if reference datatype
 *                - isList:       logical, true if list datatype
 * - unit
 * - filepath     Only relevant for FILE entities.
 * - localpath >  Only relevant for FILE insertion/update.
 * - value:       Array representing the value.  Normal arrays represent scalar values (1xN char
 *                arrays for strings, scalar arrays for other atomic value), and arrays inside a 1x1
 *                cell array represent list values.  Unset scalars are represented by a sparse
 *                array, unset list values by an empty cell array.
 * - parents:     Struct array with the following fields:
 *                - id
 *                - name
 *                - description <
 * - properties:  Struct array with the following fields:
 *                - id
 *                - name
 *                - description
 *                - importance
 *                - value
 *                - unit
 *                - datatype
 * - errors <     Struct array with the following fields:
 *                - code (as INT64)
 *                - description
 * - warnings <   Like messages.
 * - infos <      Like messages.
 *
 */

// Macros /////////////////////////////////////////////////////////////////////

namespace maoxdb {

using std::string;

// Entity handling ////////////////////////////////////////////////////////////

/**
 * @brief Convert a ResultSet to a struct mexArray.
 *
 * @return A 1xN struct array, with Entity structs as values.
 */
auto mxFromResultSet(const caosdb::transaction::ResultSet &resultSet) -> mxArray *;

/**
 * @brief Convert an Entity from libcaosdb to Octave struct mexArray.
 *
 * @return A 1x1 struct array whose entry follows the maoxdb Entity convention
 * outlined above.
 */
auto mxFromCaosDBEntity(const caosdb::entity::Entity &entity) -> mxArray *;

/**
 * @brief      Convert a DataType descriptor object to a struct array.
 *
 * @return     A 1x1 struct array with the fields (atomic_datatype, reference_name, is_list) as
 *             described in this file.
 */
auto mxFromDataType(const caosdb::entity::DataType &data_type) -> mxArray *;

/**
 * @brief Convert the libcaosdb value to a mxArray.
 *
 * @details    Null values are represented as an empty sparse array, lists are cell arrays, scalars
 *             are normal 1x1 arrays.
 */
auto mxFromValue(const caosdb::entity::Value &value) -> mxArray *;

/**
 * @brief      Set an Entity object's data (parents, properties, name etc.) from the data in ARRAY.
 *
 * @details    The id and versionId are not modified.  Either they exist already or they remain
 *             unset.
 *
 * @param      entity The entity to be updated.
 *
 * @param      array The struct array with the data.
 *
 * @param      index The index of the array from which to take the data. Default is 0.
 */
auto assignEntityDataFromMx(caosdb::entity::Entity &entity, const mxArray *array,
                            const mwSize index = 0) -> void;

/**
 * @brief      Create a vector of Entity objects from the data in ARRAY.
 *
 * @details    By default, the id and versionId are not set, this is the required state for
 *             inserting the entity into a CaosDB instance.  If `for_update` is `true`, the id and
 *             versionId are matched against the remote HEAD version and set if they are the same.
 *             If they are not the same, an exception is thrown.
 *
 *             This function is typically used for insert and update actions.
 *
 * @param      array The struct array with the data (may have more than one element).
 *
 * @param      for_update If true, assert that id and versionId match the remote HEAD and the
 *             returned Entity.
 *
 * @param      conn_name The connection name.  Same conventions hold as for other functions.
 *
 * @return     A vector with Entity objects which have their values set according to ARRAY.
 */
auto entitiesFromMx(const mxArray *array, bool for_update = false, string conn_name = "")
  -> std::vector<caosdb::entity::Entity>;

/**
 * @brief      Extract the parents from the data in ARRAY.
 *
 * @param      array The 1x1 struct array with the data, for example the "parents" field of an
 *             entity struct.
 *
 * @return     A vector with Parent objects, created from the values in ARRAY.
 */
auto parentsFromMx(const mxArray *parentsArray) -> std::vector<caosdb::entity::Parent>;

/**
 * @brief      Extract the properties from the data in ARRAY.
 *
 * @param      array The 1x1 struct array with the data, for example the "properties" field of an
 *             entity struct.
 *
 * @return     A vector with Property objects, created from the values in ARRAY.
 */
auto propertiesFromMx(const mxArray *propertiesArray) -> std::vector<caosdb::entity::Property>;

/**
 * @brief      Convert a struct array to a DataType descriptor object.
 *
 * @return     A libcaosdb DataType object.
 */
auto dataTypeFromMx(const mxArray *datatypeArray) -> caosdb::entity::DataType;

/**
 * @brief Convert the mxArray to a libcaosdb value.
 */
auto valueFromMx(const mxArray *array) -> caosdb::entity::Value;

// Exception and error handling ///////////////////////////////////////////////

/**
 * @brief      Generate a mex error message from a TransactionStatus.
 *
 * @details    Preprocess the transaction status of a transaction, to create the
 * necessary information for raising an Octave error.
 *
 * @param      trans
 *  The transaction of which to handle the status.
 *
 * @return     std::pair<std::string, std::string>
 *  The message ID and description of any error.  If the transaction status is
 *  SUCCESS, or some kind of "still running", both strings are empty.
 */
auto transactionStatusToMessage(const caosdb::transaction::Transaction *const trans)
  -> std::pair<string, string>;

/**
 * @brief Throw an Octave error if the transaction did not finish properly.
 *
 * @details On error, this function will leave this C++ context and fall back to
 * Octave.
 *
 * @param   trans
 *  The transaction to check.
 */
void throwOctExceptionIfError(const caosdb::transaction::Transaction *const trans);

/**
 * @brief Handle a CaosDB Exception and transform into error code and message.
 *
 * @details This function does all the processing to transform the information
 * in an Exception object into a form that can conveniently be handled by Octave
 * via the mexErrMsgIdAndTxt function.
 *
 * @param exc The Exception object.
 *
 * @return pair<string msgId, string msgText> The message ID and message text
 * which can be passed to Octave.
 *
 * @note Additional utility functions should be easy to implement when the
 * libcaosdb error handling changes.
 */
auto exceptionToMessage(const caosdb::exceptions::Exception &exc) -> std::pair<string, string>;

/**
 * @brief Throw a CaosDB Exception inside Octave.
 *
 * @details This function does all the processing to transform the information
 * in an Exception object into a form that is accepted by Octave's error
 * handling.d
 *
 * Internally, this function uses the exceptionToMessage function.
 *
 * @param exc The Exception object.
 */
void throwOctException(const caosdb::exceptions::Exception &exc);

// Utility functions //////////////////////////////////////////////////////////

/**
 * @brief      Merges a number of scalar (1x1) mex structs into a 1xN struct.
 *
 * @details    The content (the mxArrays behind the input structs values) is
 * duplicated.
 *
 * @param      structs: Must all have the same fields, must all be of size 1x1.
 *
 * @return     A 1xN struct array.
 */
auto mxMergeScalarStructs(const std::vector<const mxArray *> &structs) -> mxArray *;

template <typename T> auto mxScalar(T value, mxClassID class_id) -> mxArray * {
  mxArray *array = mxCreateNumericMatrix(1, 1, class_id, mxREAL);
  auto *data = static_cast<T *>(mxGetData(array));
  data[0] = value;
  return array;
}

// Convenience shortcut functions

inline auto mxScalarUINT64(UINT64_T value) -> mxArray * {
  return mxScalar<UINT64_T>(value, mxUINT64_CLASS);
}
inline auto mxScalarINT64(INT64_T value) -> mxArray * {
  return mxScalar<INT64_T>(value, mxINT64_CLASS);
}
inline auto mxScalarDOUBLE(double value) -> mxArray * { return mxCreateDoubleScalar(value); }
inline auto mxScalarLOGICAL(bool value) -> mxArray * {
  return mxCreateLogicalScalar(static_cast<mxLogical>(value));
}
inline auto mxEmptyUINT64() -> mxArray * {
  mxArray *array = mxCreateNumericMatrix(0, 0, mxUINT64_CLASS, mxREAL);
  return array;
}
inline auto mxEmptyINT64() -> mxArray * {
  mxArray *array = mxCreateNumericMatrix(0, 0, mxINT64_CLASS, mxREAL);
  return array;
}
inline auto mxEmptyDOUBLE() -> mxArray * {
  mxArray *array = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
  return array;
}
inline auto mxEmptyLOGICAL() -> mxArray * {
  mxArray *array = mxCreateLogicalMatrix(0, 0);
  return array;
}
inline auto mxEmptyCHAR() -> mxArray * {
  mxArray *array = mxCreateCharArray(0, (mwSize const *)nullptr);
  return array;
}
inline auto mxEmptySTRING() -> mxArray * {
  mxArray *array = mxCreateString("");
  return array;
}
inline auto mxEmptySparse() -> mxArray * {
  mxArray *array = mxCreateSparse(0, 0, 0, mxREAL);
  return array;
}
inline auto mxEmptyStruct() -> mxArray * {
  mxArray *array = mxCreateStructArray(0, (mwSize const *)nullptr, 0, (char const **)nullptr);
  return array;
}
inline auto mxEmptyCell() -> mxArray * {
  mxArray *array = mxCreateCellMatrix(0, 0);
  return array;
}

/**
 * @brief A workaround for https://savannah.gnu.org/bugs/?61280
 *
 * @details Valgrind complains when duplicating empty sparse arrays.  This implementation works
 * around the bug, but does not care about sparse arrays inside cell arrays yet.
 */
auto mxDuplicateArrayAlsoSparse(mxArray *array) -> mxArray *;

/**
 * @brief      Get the value of a scalar array.
 */
template <typename T> inline auto mxGetScalarValue(mxArray *scalar) -> T {
  return *(static_cast<T *>(mxGetData(scalar)));
}

/**
 * @brief      Create a struct array from a string container.
 *
 * @details    Just a convenience wrapper for C++ data structures.
 *
 * @param      fields A container of string-convertibles.  Must support data() and size().
 *
 * @param      fields A container of ints.  Must support data() and size().
 *
 * @return     The struct array
 */
template <typename FieldContainer, typename DimsContainer>
auto mxCreateStructFromStrings(const FieldContainer &fields, const DimsContainer &dims)
  -> mxArray * {
  auto char_fields = std::vector<const char *>();
  auto alloc = std::allocator<char>();
  for (auto field : fields) {
    std::string field_str(field);
    auto *data = alloc.allocate(field_str.size() + 1);
    std::strcpy(data, field_str.c_str());
    char_fields.push_back(data);
  }
  auto result =
    mxCreateStructArray(dims.size(), dims.data(), char_fields.size(), char_fields.data());
  // TODO (dh) Memory is not freed afterwards.
  return result;
}

/**
 * @brief      Create 1x1 a struct array from a string container.
 *
 * @details    Just a convenience wrapper for C++ data structures.
 *
 * @param      fields A container of string-convertibles.  Must support data() and size().
 *
 * @return     The struct array
 */
template <typename FieldContainer>
auto mxCreateStructFromStrings(const FieldContainer &fields) -> mxArray * {
  const std::array<mwSize, 2> dims = {1, 1};
  return mxCreateStructFromStrings(fields, dims);
}

/**
 * @brief      Extract a std::string from a char array
 *
 * @details    Handles the length check
 *
 * @param      array The char array from which the string shall be extracted.
 *
 * @return     The std::string.
 */
auto mxGetStdString(const mxArray *array) -> std::string;

/**
 * @brief      Convert the mx cell string array to a string vector.
 */
auto mxCellToStrings(const mxArray *array) -> std::vector<std::string>;

/**
 * @brief      Convert the enum to an appropriate string array.
 *
 * @details    There must be a specialization upstream for each enum type.
 */
template <typename Enum> auto mxEnumToString(Enum enum_value) -> mxArray * {
  auto const name = caosdb::utility::getEnumNameFromValue(enum_value);
  return mxCreateString(name.c_str());
}

/**
 * @brief      Convert the array to an enum.
 *
 * @details    There must be a specialization upstream for each enum type.
 */
template <typename Enum> auto mxStringToEnum(const mxArray *const array) -> Enum {
  if (mxIsEmpty(array)) {
    auto value = static_cast<Enum>(0); // attempt to use the default value
    return value;
  }
  Enum value = caosdb::utility::getEnumValueFromName<Enum>(mxGetStdString(array));
  return value;
}

} // namespace maoxdb

#endif /* MAOXDB_H */
