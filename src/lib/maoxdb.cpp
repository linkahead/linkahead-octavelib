/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/**
 * Mostly utility functions for MEX files interacting with libcaosdb.
 */

#include "maoxdb.hpp"
#include "mex.h"
#include "caosdb/connection.h"
#include "caosdb/logging.h" // for CAOSDB_LOG_TRACE
#include "caosdb/status_code.h"
#include "caosdb/transaction.h"
#include "caosdb/transaction_status.h"
#include <boost/lexical_cast.hpp>
#include <map>
#include <type_traits>

namespace maoxdb {
using std::string;

namespace ce = caosdb::entity;
namespace cu = caosdb::utility;

const auto logger_name = string("maoxdb");

auto mxFromCaosDBParents(const ce::Parents &parents) -> mxArray *;
auto mxFromCaosDBProperties(const ce::Properties &properties) -> mxArray *;
auto mxFromCaosDBMessages(const ce::Messages &messages) -> mxArray *;

// Exception and error handling ///////////////////////////////////////////////

/**
 * Generate a mex error message from a TransactionStatus.
 */
auto transactionStatusToMessage(const caosdb::transaction::Transaction *const trans)
  -> std::pair<string, string> {
  const auto &status = trans->GetStatus();
  const auto &code = status.GetCode();
  // Don't raise errors if it's only transaction errors (put into entity
  // messages).
  if (!status.IsError() || code == caosdb::StatusCode::GENERIC_TRANSACTION_ERROR) {
    if (code == caosdb::StatusCode::GENERIC_TRANSACTION_ERROR) {
      CAOSDB_LOG_INFO(logger_name) << std::to_string(code) << " / " << status.GetDescription();
    }
    return std::pair<string, string>();
  }
  string id = string("maoxdb:E") + std::to_string(code);
  string text = status.GetDescription();
  CAOSDB_LOG_INFO(logger_name) << id << " / " << text;
  return std::pair<string, string>(id, text);
}

/**
 * Throw an Octave error if the transaction did not finish properly.
 */
void throwOctExceptionIfError(const caosdb::transaction::Transaction *const trans) {
  auto msg = maoxdb::transactionStatusToMessage(trans);
  if (msg.first != "") {
    CAOSDB_LOG_TRACE(logger_name) << msg.first << " // " << msg.second;
    caosdb::utility::reset_arena();
    mexErrMsgIdAndTxt(msg.first.c_str(), msg.second.c_str());
  }
}

/**
 * Extract useful strings from an Exception: ID and description.
 */
auto exceptionToMessage(const caosdb::exceptions::Exception &exc) -> std::pair<string, string> {

  string id = std::to_string(exc.GetCode());
  string text = caosdb::get_status_description(exc.GetCode()) + "\n" + exc.what();

  return std::pair<string, string>(id, text);
}

/**
 * Just throw the error.
 */
void throwOctException(const caosdb::exceptions::Exception &exc) {
  std::pair<string, string> excContent = exceptionToMessage(exc);
  caosdb::utility::reset_arena();
  mexErrMsgIdAndTxt(excContent.first.c_str(), excContent.second.c_str());
}
///////////////////////////////////////////////////////////////////////////////
// Entity handling ////////////////////////////////////////////////////////////

////////// libcaosdb to mxArray ///////////////////////////////////////////////

/**
 * @brief Convert a ResultSet to a struct mexArray.
 */
auto mxFromResultSet(const caosdb::transaction::ResultSet &resultSet) -> mxArray * {
  if (resultSet.size() == 0) {
    auto *result = mxEmptyStruct();
    return result;
  }
  std::vector<const mxArray *> entities;
  // Obtain entities
  for (const auto &entity : resultSet) {
    entities.push_back(mxFromCaosDBEntity(entity));
  }

  auto *result = mxMergeScalarStructs(entities);
  return result;
}

/**
 * @brief Convert an Entity from libcaosdb to Octave struct mexArray.
 */
auto mxFromCaosDBEntity(const ce::Entity &entity) -> mxArray * {
  // clang-format off
  auto fields = std::vector<const char*>
    {"role",
     "id",
     "versionId",
     "name",
     "description",
     "datatype",
     "unit",
     "value",
     "parents",
     "properties",
     "errors",
     "warnings",
     "infos"
    };
  // clang-format on

  CAOSDB_LOG_DEBUG(logger_name + "::mxFromCaosDBEntity") << entity.ToString();
  auto *result = mxCreateStructFromStrings(fields);
  // Fill with scalar values
  mxSetField(result, 0, "role", mxEnumToString<ce::Role>(entity.GetRole()));
  mxSetField(result, 0, "id", mxCreateString(entity.GetId().c_str()));
  mxSetField(result, 0, "versionId", mxCreateString(entity.GetVersionId().c_str()));
  mxSetField(result, 0, "name", mxCreateString(entity.GetName().c_str()));
  mxSetField(result, 0, "description", mxCreateString(entity.GetDescription().c_str()));
  mxSetField(result, 0, "datatype", mxFromDataType(entity.GetDataType()));
  mxSetField(result, 0, "unit", mxCreateString(entity.GetUnit().c_str()));
  mxSetField(result, 0, "value", mxFromValue(entity.GetValue()));

  // Parents and Properties
  mxSetField(result, 0, "parents", mxFromCaosDBParents(entity.GetParents()));
  mxSetField(result, 0, "properties", mxFromCaosDBProperties(entity.GetProperties()));

  // message type content
  mxSetField(result, 0, "errors", mxFromCaosDBMessages(entity.GetErrors()));
  mxSetField(result, 0, "warnings", mxFromCaosDBMessages(entity.GetWarnings()));
  mxSetField(result, 0, "infos", mxFromCaosDBMessages(entity.GetInfos()));

  return result;
}

// Parents to struct array
auto mxFromCaosDBParents(const ce::Parents &parents) -> mxArray * {
  // clang-format off
  std::vector<const char*> fields =
    {"id",
     "name",
     "description"
    };
  // clang-format on
  std::array<mwSize, 2> dims = {1, (mwSize)parents.size()};
  auto *result = mxCreateStructFromStrings(fields, dims);
  for (size_t i = 0; i < parents.size(); ++i) {
    auto parent = parents.at(i);
    mxSetField(result, i, "id", mxCreateString(parent.GetId().c_str()));
    mxSetField(result, i, "name", mxCreateString(parent.GetName().c_str()));
    mxSetField(result, i, "description", mxCreateString(parent.GetDescription().c_str()));
  }
  return result;
}

// Properties to struct array
auto mxFromCaosDBProperties(const ce::Properties &properties) -> mxArray * {
  // clang-format off
  std::vector<const char*> fields =
    {"id",
     "name",
     "description",
     "importance",
     "value",
     "unit",
     "datatype"
    };
  // clang-format on
  std::array<mwSize, 2> dims = {1, (mwSize)properties.size()};
  auto *result = mxCreateStructFromStrings(fields, dims);
  for (mwIndex i = 0; i < properties.size(); ++i) {
    auto &property = properties.at(i);
    mxSetField(result, i, "id", mxCreateString(property.GetId().c_str()));
    mxSetField(result, i, "name", mxCreateString(property.GetName().c_str()));
    mxSetField(result, i, "description", mxCreateString(property.GetDescription().c_str()));
    mxSetField(result, i, "importance", mxEnumToString<ce::Importance>(property.GetImportance()));
    mxSetField(result, i, "unit", mxCreateString(property.GetUnit().c_str()));
    mxSetField(result, i, "datatype", mxFromDataType(property.GetDataType()));
    mxSetField(result, i, "value", mxFromValue(property.GetValue()));
  }
  return result;
}

// Messages to struct array
auto mxFromCaosDBMessages(const ce::Messages &messages) -> mxArray * {
  std::vector<const char *> fields = {
    "code",       //
    "description" //
  };
  std::array<mwSize, 2> dims = {1, (mwSize)messages.size()};
  auto *result = mxCreateStructFromStrings(fields, dims);
  for (mwIndex i = 0; i < messages.size(); ++i) {
    const auto &message = messages.at(i);
    mxSetField(result, i, "code", mxScalarINT64(message.GetCode()));
    mxSetField(result, i, "description", mxCreateString(message.GetDescription().c_str()));
  }
  return result;
}

/**
 * Convert a DataType descriptor object to a struct array.
 */
auto mxFromDataType(const ce::DataType &data_type) -> mxArray * {
  namespace ce = ce;

  mxArray *dtype_name;
  auto is_list = mxScalarLOGICAL(data_type.IsList());
  auto is_reference = mxScalarLOGICAL(data_type.IsReference());
  if (data_type.IsList()) {
    const auto &list_datatype = data_type.GetAsList();
    is_reference = mxScalarLOGICAL(list_datatype.IsListOfReference());
    if (list_datatype.IsListOfReference()) {
      dtype_name = mxCreateString(list_datatype.GetReferenceDataType().GetName().c_str());
    } else if (list_datatype.IsListOfAtomic()) {
      dtype_name = mxEnumToString<ce::AtomicDataType>(list_datatype.GetAtomicDataType());
    } else {
      throw std::logic_error(string("Unexpected list data type: " + data_type.ToString()));
    }
  } else if (data_type.IsReference()) {
    dtype_name = mxCreateString(data_type.GetAsReference().GetName().c_str());
  } else if (data_type.IsAtomic()) {
    dtype_name = mxEnumToString<ce::AtomicDataType>(data_type.GetAsAtomic());
  } else if (data_type.IsUndefined()) {
    return mxEmptySparse(); // Workaround to denote a Null value: sparse array
  } else {
    throw std::logic_error(string("Unexpected data type: " + data_type.ToString()));
  }

  // clang-format off
  auto fields = std::vector<const char*>
    {"dtypeName",
     "isReference",
     "isList"
    };
  // clang-format on
  auto *result = mxCreateStructFromStrings(fields);
  mxSetField(result, 0, "dtypeName", dtype_name);
  mxSetField(result, 0, "isReference", is_reference);
  mxSetField(result, 0, "isList", is_list);

  return result;
}

/**
 * Convert the libcaosdb value to a mxArray.
 */
auto mxFromValue(const ce::Value &value) -> mxArray * {
  if (value.IsNull()) {
    return mxEmptySparse();      // Workaround to denote a Null value: sparse array
  } else if (value.IsString()) { // Scalars are converted to normal arrays.
    return mxCreateString(value.GetAsString().c_str());
  } else if (value.IsDouble()) {
    return mxScalarDOUBLE(value.GetAsDouble());
  } else if (value.IsBool()) {
    return mxScalarLOGICAL(value.GetAsBool());
  } else if (value.IsInt64()) {
    return mxScalarINT64(value.GetAsInt64());
  } else if (!value.IsVector()) {
    throw std::logic_error(string("Unexpected value type: " + value.ToString()));
  }
  // It's a list now -> convert to cell array always.
  auto list = value.GetAsVector();
  auto *result = mxCreateCellArray(0, (mwSize *)nullptr);
  if (list.empty()) { // Empty list: empty cell array
    return result;
  }
  const std::array<mwSize, 2> dims = {1, (mwSize)list.size()};
  auto list_value = list[0];
  if (list_value.IsString()) { // String
    result = mxCreateCellArray(2, dims.data());
    for (size_t i = 0; i < list.size(); ++i) {
      mxSetCell(result, i, mxCreateString(list[i].GetAsString().c_str()));
    }
  } else if (list_value.IsBool()) { // Bool
    result = mxCreateLogicalArray(2, dims.data());
    auto *data = static_cast<mxLogical *>(mxGetData(result));
    for (size_t i = 0; i < list.size(); ++i) {
      data[i] = list[i].GetAsBool();
    }
  } else if (list_value.IsInt64()) { // Int
    result = mxCreateNumericArray(2, dims.data(), mxINT64_CLASS, mxREAL);
    auto *data = static_cast<INT64_T *>(mxGetData(result));
    for (size_t i = 0; i < list.size(); ++i) {
      data[i] = list[i].GetAsInt64();
    }
  } else if (list_value.IsDouble()) { // Double
    result = mxCreateNumericArray(2, dims.data(), mxDOUBLE_CLASS, mxREAL);
    auto *data = static_cast<double *>(mxGetData(result));
    for (size_t i = 0; i < list.size(); ++i) {
      data[i] = list[i].GetAsDouble();
    }
  } else {
    throw std::logic_error("Unexpected list value type");
  }
  // Non-empty list: 1x1 cell array with the payload at first position.
  const std::array<mwSize, 2> wrapper_dims = {1, 1};
  auto *result_wrapper = mxCreateCellArray(0, wrapper_dims.data());
  mxSetCell(result_wrapper, 0, result);
  return result_wrapper;
}

////////// mxArray to libcaosdb ///////////////////////////////////////////////

/*
 * Set an Entity object's data (parents, properties, name etc.) from the data in ARRAY.
 */
void assignEntityDataFromMx(ce::Entity &entity, const mxArray *array, const mwSize index) {
  CAOSDB_LOG_TRACE(logger_name) << "assignEntityDataFromMx";
  CAOSDB_LOG_TRACE(logger_name) << "type: " << mxGetClassID(array);
  entity.SetRole(mxStringToEnum<ce::Role>(mxGetField(array, index, "role")));
  CAOSDB_LOG_TRACE(logger_name) << "Role set";
  entity.SetName(mxGetStdString(mxGetField(array, index, "name")));
  entity.SetDescription(mxGetStdString(mxGetField(array, index, "description")));
  entity.SetDataType(dataTypeFromMx(mxGetField(array, index, "datatype")));
  entity.SetUnit(mxGetStdString(mxGetField(array, index, "unit")));
  entity.SetValue(valueFromMx(mxGetField(array, index, "value")));
  if (entity.GetRole() == ce::Role::FILE) { // This is necessary until caosdb-server#174 is fixed.
    entity.SetFilePath(mxGetStdString(mxGetField(array, index, "filepath")));
    entity.SetLocalPath(mxGetStdString(mxGetField(array, index, "localpath")));
  }
  CAOSDB_LOG_TRACE(logger_name) << "parents & properties";
  for (size_t i = entity.GetParents().size(); i > 0; i--) {
    entity.RemoveParent(i - 1);
  }
  for (auto parent : parentsFromMx(mxGetField(array, index, "parents"))) {
    entity.AppendParent(parent);
  }
  for (size_t i = entity.GetProperties().size(); i > 0; i--) {
    entity.RemoveProperty(i - 1);
  }
  for (auto &property : propertiesFromMx(mxGetField(array, index, "properties"))) {
    entity.AppendProperty(property);
  }
}

/*
 * Create a vector of Entity objects from the data in ARRAY.
 */
auto entitiesFromMx(const mxArray *array, bool for_update, string conn_name)
  -> std::vector<ce::Entity> {
  auto numel = mxGetNumberOfElements(array);
  CAOSDB_LOG_TRACE(logger_name) << "entitiesFromMx : numel : " << numel;
  std::vector<ce::Entity> entities(numel); // Vector with empty entities
  if (numel == 0) {
    return entities;
  }
  // for_update: Retrieve entities and assign everything but ID ///////////////
  if (for_update) {
    CAOSDB_LOG_TRACE(logger_name) << "This is for updating.";
    std::shared_ptr<caosdb::connection::Connection> connection;
    if (conn_name.empty()) {
      connection = caosdb::connection::ConnectionManager::GetDefaultConnection();
    } else {
      connection = caosdb::connection::ConnectionManager::GetConnection(conn_name);
    }
    std::map<string, mwSize> idMap; // To get the Entity data for each ID
    auto transaction = connection->CreateTransaction();
    for (mwSize i = 0; i < numel; ++i) {
      auto id = mxGetStdString(mxGetField(array, i, "id"));
      idMap[id] = i; // map ID -> index in mxArray [0..numel-1]
      transaction->RetrieveById(id);
    }
    CAOSDB_LOG_TRACE(logger_name) << "exec async";
    transaction->ExecuteAsynchronously();
    CAOSDB_LOG_TRACE(logger_name) << "waitforit";
    auto t_stat = transaction->WaitForIt();
    CAOSDB_LOG_TRACE(logger_name) << "waited: " << t_stat.GetCode() << " / "
                                  << t_stat.GetDescription();
    maoxdb::throwOctExceptionIfError(transaction.get());
    CAOSDB_LOG_TRACE(logger_name) << "status: " << transaction->GetStatus().GetDescription();
    const auto &results = transaction->GetResultSet();
    // Update the Entity contents
    CAOSDB_LOG_TRACE(logger_name) << "&results " << &results;
    CAOSDB_LOG_TRACE(logger_name) << "numel results " << results.size();
    entities = std::vector<ce::Entity>();
    for (ce::Entity entity : results) {
      auto index = idMap[entity.GetId()];
      auto remote_version = entity.GetVersionId();
      auto local_version = mxGetStdString(mxGetField(array, index, "versionId"));
      if (remote_version != local_version) {
        throw std::invalid_argument(
          "Version is not at HEAD of remote, but this is required for an update action.");
      }
      assignEntityDataFromMx(entity, array, index);
      entities.push_back(entity);
    }
  }
  // insert only: No ID, only assign other values ///////////////////////////
  else {
    CAOSDB_LOG_TRACE(logger_name) << "This is for inserting.";
    for (mwSize i = 0; i < numel; ++i) {
      assignEntityDataFromMx(entities[i], array, i);
    }
  }
  return entities;
}

/*
 * Extract the parents from the data in ARRAY.
 */
auto parentsFromMx(const mxArray *parentsArray) -> std::vector<ce::Parent> {
  std::vector<ce::Parent> parents(mxGetNumberOfElements(parentsArray));
  for (mwSize i = 0; i < mxGetNumberOfElements(parentsArray); ++i) {
    parents[i].SetId(mxGetStdString(mxGetField(parentsArray, i, "id")));
    parents[i].SetName(mxGetStdString(mxGetField(parentsArray, i, "name")));
  }
  return parents;
}

/*
 * Extract the properties from the data in ARRAY.
 */
auto propertiesFromMx(const mxArray *propertiesArray) -> std::vector<ce::Property> {
  std::vector<ce::Property> properties(mxGetNumberOfElements(propertiesArray));
  for (mwSize i = 0; i < mxGetNumberOfElements(propertiesArray); ++i) {
    properties[i].SetId(mxGetStdString(mxGetField(propertiesArray, i, "id")));
    properties[i].SetName(mxGetStdString(mxGetField(propertiesArray, i, "name")));
    properties[i].SetDescription(mxGetStdString(mxGetField(propertiesArray, i, "description")));
    properties[i].SetImportance(
      mxStringToEnum<ce::Importance>(mxGetField(propertiesArray, i, "importance")));
    properties[i].SetDataType(dataTypeFromMx(mxGetField(propertiesArray, i, "datatype")));
    properties[i].SetUnit(mxGetStdString(mxGetField(propertiesArray, i, "unit")));
    properties[i].SetValue(valueFromMx(mxGetField(propertiesArray, i, "value")));
  }
  return properties;
}

/**
 * Convert a struct array to a DataType descriptor object.
 */
auto dataTypeFromMx(const mxArray *datatypeArray) -> ce::DataType {
  if (mxIsEmpty(datatypeArray)) {
    return ce::DataType();
  }
  CAOSDB_LOG_TRACE(logger_name) << "dataTypeFromMx";
  if (!mxIsStruct(datatypeArray)) {
    throw std::logic_error(string("Unexpected type for datatype (Class ID ") +
                           boost::lexical_cast<std::string>(mxGetClassID(datatypeArray)) +
                           ", expected " +
                           boost::lexical_cast<std::string>(mxClassID::mxSTRUCT_CLASS) + ")");
  }
  auto is_list = mxGetScalarValue<bool>(mxGetField(datatypeArray, 0, "isList"));
  auto is_reference = mxGetScalarValue<bool>(mxGetField(datatypeArray, 0, "isReference"));
  auto dtype_name = mxGetStdString(mxGetField(datatypeArray, 0, "dtypeName"));

  ce::DataType dtype;
  if (is_reference && is_list) {
    dtype = ce::DataType::ListOf(dtype_name);
  } else if (is_reference) {
    dtype = ce::DataType(dtype_name);
  } else if (is_list) {
    dtype = ce::DataType::ListOf(cu::getEnumValueFromName<ce::AtomicDataType>(dtype_name));
  } else {
    dtype = ce::DataType(cu::getEnumValueFromName<ce::AtomicDataType>(dtype_name));
  }

  return dtype;
}

/**
 * Convert the mxArray to a libcaosdb value.
 */
auto valueFromMx(const mxArray *array) -> caosdb::entity::Value {
  using ce::Value;
  auto result = Value();
  size_t numel;
  if (mxIsSparse(array)) { // Null value
    return result;
  } else if (mxIsCell(array)) { // list value (in cell array)
    if (mxIsEmpty(array)) {
      // Empty list
      return Value(std::vector<int>());
    }
    array = mxGetCell(array, 0);
    numel = mxGetNumberOfElements(array);
    if (mxIsCell(array)) { // string cell array
      auto content = std::vector<string>(numel);
      for (size_t i = 0; i < numel; ++i) {
        auto value = mxGetStdString(mxGetCell(array, i));
        content[i] = value;
      }
      result = Value(content);
    } else if (mxIsLogical(array)) { // bool
      auto content = std::vector<bool>(numel);
      auto data = static_cast<bool *>(mxGetData(array));
      for (size_t i = 0; i < numel; ++i) {
        content[i] = data[i];
      }
      result = Value(content);
    } else if (mxIsInt64(array)) { // int
      auto content = std::vector<INT64_T>(numel);
      auto data = static_cast<INT64_T *>(mxGetData(array));
      for (size_t i = 0; i < numel; ++i) {
        content[i] = data[i];
      }
      result = Value(content);
    } else if (mxIsDouble(array)) { // double
      auto content = std::vector<double>(numel);
      auto data = static_cast<double *>(mxGetData(array));
      for (size_t i = 0; i < numel; ++i) {
        content[i] = data[i];
      }
      result = Value(content);
    } else {
      throw std::logic_error(string("Unexpected array type for list value (Class ID ") +
                             boost::lexical_cast<std::string>((mxGetClassID(array))) + ")");
    }
  } else {                 // scalar value (no cell array)
    if (mxIsChar(array)) { // string
      result = Value(mxGetStdString(array));
    } else if (mxIsLogical(array)) { // bool
      auto data = *static_cast<bool *>(mxGetData(array));
      result = Value(data);
    } else if (mxIsInt64(array)) { // int
      auto data = *static_cast<INT64_T *>(mxGetData(array));
      result = Value(data);
    } else if (mxIsDouble(array)) { // double
      auto data = *static_cast<double *>(mxGetData(array));
      result = Value(data);
    } else {
      throw std::logic_error(string("Unexpected type for scalar value (Class ID ") +
                             boost::lexical_cast<std::string>((mxGetClassID(array))) + ")");
    }
  }

  return result;
}

///////////////////////////////////////////////////////////////////////////////
// Utility functions //////////////////////////////////////////////////////////

/* @brief A workaround for https://savannah.gnu.org/bugs/?61280 */
auto mxDuplicateArrayAlsoSparse(mxArray *array) -> mxArray * {
  if (mxIsStruct(array)) {
    auto field_names = std::vector<const char *>();
    for (mwSize i = 0; i < mxGetNumberOfFields(array); ++i) {
      field_names.push_back(mxGetFieldNameByNumber(array, i));
    }
    auto duplicate = mxCreateStructArray(mxGetNumberOfDimensions(array), mxGetDimensions(array),
                                         mxGetNumberOfFields(array), field_names.data());
    for (mwIndex f = 0; f < mxGetNumberOfFields(array); ++f) {
      for (mwIndex i = 0; i < mxGetNumberOfElements(array); ++i) {
        auto val = mxGetFieldByNumber(array, i, f);

        mxSetFieldByNumber(duplicate, i, f,
                           mxDuplicateArrayAlsoSparse(mxGetFieldByNumber(array, i, f)));
      }
    }
    return duplicate;
  }
  if (mxIsSparse(array) && mxGetM(array) == 0 && mxGetN(array) == 0) {
    return mxEmptySparse();
  } else {
    return mxDuplicateArray(array);
  }
}

/*
 * @brief      Merges a number of scalar mex structs into a 1xN struct.
 */
auto mxMergeScalarStructs(const std::vector<const mxArray *> &structs) -> mxArray * {

  // We need the field names first to create a new struct
  auto nFields = (mwSize)mxGetNumberOfFields(structs[0]);
  auto fields = std::vector<const char *>(nFields);
  for (mwIndex i = 0; i < nFields; ++i) {
    fields[i] = mxGetFieldNameByNumber(structs[0], i);
  }

  auto dims = std::array<mwSize, 2>{1, (mwSize)structs.size()};
  auto result = mxCreateStructFromStrings(fields, dims);

  auto i = mwIndex(0) - 1;
  for (auto scalarStruct : structs) {
    ++i;
    for (auto field : fields) {
      mxSetField(result, i, field, mxDuplicateArrayAlsoSparse(mxGetField(scalarStruct, 0, field)));
    }
  }
  return result;
}

/*
 * @brief      Extract a string from a char array
 */
auto mxGetStdString(const mxArray *array) -> string {
  if (array == nullptr) {
    std::cerr << "Trying to convert a null pointer to std::string." << std::endl;
  }
  auto len = mxGetNumberOfElements(array) + 1; // NUL byte
  auto buf = std::vector<char>(len);
  auto ret = mxGetString(array, buf.data(), len);
  if (ret) {
    if (len == 1) {
      return string();
    }
    throw std::invalid_argument("Could not extract a string, probably wrong mxArray content.");
  }
  return string(buf.data());
}

/*
 * @brief      Convert the mx cell string array to a string vector.
 */
auto mxCellToStrings(const mxArray *array) -> std::vector<std::string> {
  if (!mxIsCell(array)) {
    throw std::invalid_argument("Must be a cell array of strings.");
  }
  std::vector<string> result(mxGetNumberOfElements(array));
  for (mwIndex i = 0; i < mxGetNumberOfElements(array); ++i) {
    result[i] = mxGetStdString(mxGetCell(array, i));
  }
  return result;
}

} // namespace maoxdb
