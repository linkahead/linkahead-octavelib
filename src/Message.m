% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

classdef Message < handle

  properties
    code
    description
  end

  methods

    % Constructor.
    % The structure of the DATA parameter follows the convention outline in `maoxdb.hpp`.
    function obj = Message(data)
      narginchk(1, 1);
      obj.code = data.code;
      obj.description = data.description;
    end

    function ans = str(obj)
      ans = [num2str(obj.code), " - ", obj.description];
    end

    function disp(obj)
      disp(obj.str());
    end

  end
end
