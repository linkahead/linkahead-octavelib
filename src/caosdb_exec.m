% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This file is a part of the CaosDB Project.
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

% -*- texinfo -*-
% @deftypefn {Function File} {@var{out} =} caosdb_exec (arg)
% @cindex index term
% Calls the equivalent of the @code{caosdb} command line interface executable.  Mostly, this can
% output the version of libcaosdb.
%
% Typical arguments are @code{"--version"} or @code{"--test-connection"}.
% @end deftypefn
function out = caosdb_exec(arg)
  out = maox_caosdb(arg);
end
