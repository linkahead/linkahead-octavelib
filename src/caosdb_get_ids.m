% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

%% Get the IDs of the given entities.
%
% This is a convenience function, because often cell arrays with Entity objects are returned, but
% only the IDs are needed.
%
% Parameters
% ----------
%
% entities : cell array
%  The Entity objects.
%
% Returns
% -------
%
% out : string cell array
%  The IDs, as strings in a cell array of the same shape as ENTITIES.
function out = caosdb_get_ids(entities)
  out = cellfun(@(x)(x.id), entities, "UniformOutput", false);
end
