% This file is a part of the CaosDB Project.
%
% Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
% Copyright (C) 2021 Daniel Hornung <d.hornung@indiscale.com>
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program. If not, see <https://www.gnu.org/licenses/>.

classdef Property < handle

  properties
    id
    name
    description
    importance
    unit
    value
  end
  properties (Access = private)
    datatype_    % 1x1 struct with the following fields:
    %  - isList         logical
    %  - isReference    logical
    %  - dtypeName      string.  Either the atomic datatype or the reference name.
  end

  methods

    % Constructor.
    % The structure of the DATA parameter follows the convention outline in `maoxdb.hpp`.
    function obj = Property(data)
      narginchk(0, 1);
      if nargin == 1
        obj.id = data.id;
        obj.name = data.name;
        obj.description = data.description;
        obj.importance = data.importance;
        obj.datatype_ = data.datatype;
        obj.unit = data.unit;
        obj.value = data.value;
      end
    end

    % Convert to a struct which has all the fields that may be needed for interaction with the
    % maoxdb library.
    function struct_array = to_struct(obj)
      res = struct();
      % Scalars first
      res.id = obj.id;
      res.name = obj.name;
      res.description = obj.description;
      res.importance = obj.importance;
      res.datatype = obj.datatype_;
      res.unit = obj.unit;
      if isempty(obj.datatype_)
        if ~isempty(obj.value)
          warning(["Trying to transmit an Entity with value but without Datatype.  List-iness", ...
                   " is unknown, so the value will be omitted."]);
        end
        % res.datatype = struct("dtypeName", "UNSPECIFIED", "isList", false, "isReference", false);
        res.value = sparse([]);
      else
        res.value = maox_pack_value(obj.value, obj.datatype_.isList);
      end
      if isempty(res.value) && ~ischar(res.value)
        res.value = sparse([]);
      end

      struct_array = res;
    end

    function disp(obj)
      disp(obj.to_struct());
    end

    % Getters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function result = get_datatype(obj)
      result = obj.datatype_;
    end

    % Setters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function set_datatype(obj, dtype_name, is_reference, is_list)
      if nargin < 4
        is_list = false;
      end
      if nargin < 3
        is_reference = false;
      end
      assert(nargin >= 2, "maox:InvalidArgument", "Need at least an atomic datatype.");

      dtype.isList = is_list;
      dtype.isReference = is_reference;
      if ~is_reference
        assert(any(strcmp({ "UNSPECIFIED", "TEXT", "DOUBLE", "DATETIME", "INTEGER", "BOOLEAN"}, ...
                          dtype_name)), ...
               "maox:InvalidArgument", ["Unrecognized atomic datatype: " dtype_name]);
      end
      dtype.dtypeName = dtype_name;
      obj.datatype_ = dtype;
    end

  end
end
