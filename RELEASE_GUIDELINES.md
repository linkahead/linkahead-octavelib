# Release Guidelines for the CaosDB Octave Client Library

This document specifies release guidelines in addition to the general release
guidelines of the CaosDB Project
([RELEASE_GUIDELINES.md](https://gitlab.com/caosdb/caosdb/blob/dev/RELEASE_GUIDELINES.md))

## General Prerequisites

* All tests are passing.
* CHANGELOG.md is up-to-date.
* DEPENDENCIES.md is up-to-date.

## Steps

1. Create a release branch from the dev branch. This prevents further changes
   to the code base and a never ending release process. Naming: `release-<VERSION>`

2. Update CHANGELOG.md:
   1. Rename the *Unreleased* section to the new version.
   2. Remove empty subsections.

3. Check all general prerequisites.

4. Check version numbers and dates:
   1. In `doc/conf.py`.
   2. In `DESCRIPTION`.
   3. Date in `doc/Generate_Octave_Doc.m`.

5. Merge the release branch into the main branch.

6. Tag the latest commit of the main branch with `v<VERSION>`.

7. Create release in Gitlab:
   1. On [Releases](https://gitlab.indiscale.com/caosdb/src/caosdb-octavelib/-/releases), click "New release"
   2. Choose tag, type release title (same as tag), no milestone, possibly short release note, no
      manual assets are necessary, possibly link to documentation.
      1. Release title: `v0.2.0` (replace by actual release)
      2. Milestones: None
      3. Release date: today
      4. Release notes (replace release in link, copy from changelog file):
         ```
# Changelog #

See [full changelog](https://gitlab.indiscale.com/caosdb/src/caosdb-octavelib/-/blob/v0.2.0/CHANGELOG.md).

### Changed ###

(Copy & paste from CHANGELOG.md)
```
      5. Assets:
        - URL: https://docs.indiscale.com/caosdb-octavelib/
        - Link title: Documentation
        - Type: Other

8. Delete the release branch.

9. Merge the main branch back into the dev branch.

10. After the merge of main to dev, start a new development version by increasing at least the
    `MICRO` version.
    1. Re-add an *Unreleased* section to the CHANGELOG.md, with all subsections.
    2. Increase version number in `doc/conf.py` and `DESCRIPTION`

## Upstream package ##

These steps are necessary for creating the upstream package at Octave's repository:

1. Update Octave-package repository on github.com:
   1. In `packages/caosdb.yaml`, create new version release:
      1. Set `id`: same as tag
      2. Set `date`: today
      3. Set `url`: Use the asset from the Gitlab release, for example [this url](https://gitlab.indiscale.com/caosdb/src/caosdb-octavelib/-/archive/v0.0.1/caosdb-octavelib-v0.0.1.tar.gz).
      4. Set `sha256`: Download the asset and run `sha256` on it:  
         `curl https://gitlab.indiscale.com/[...].tar.bz2 | sha256sum`
   2. Commit with commit message "Package release CaosDB-octavelib 0.2.1"
   3. Create pull request at https://github.com/gnu-octave/packages and accept it:
      - Assign to myself.
      - Set label `package release`.
      - Ignore failing test `check-package` if the error is:  
        ```
        [EE] The build tool `conan' seems to be missing.
        ```
      - Merge pull request. 
